''' @file     fibonacci.py
    @author   Austin Fong
'''
def fib (idx):
    '''
    @brief       This function calculates a Fibonacci number at a specific index.
    @param idx   An integer specifying the index of the desired Fibonacci number
    '''
    # Convert the user input into an integer
    idx = int(idx)
    # Account for edge cases where the index is equal to 0, 1 or 
    # a negative number.
    if idx == 0:
        return 0
    if idx == 1:
        return 1
    if idx < 0:
        return "invalid, index must be a positive integer"
    # Run the following code if index is not an edge case
    if idx > 1:
    # Set the first two Fibonacci sequence numbers
        n1 = 0
        n2 = 1
    # Run a for loop based on the size of the index number
        for x in range(idx-1):
    # Add the two previous Fibonacci numbers
            total = n1 + n2
    # Set the two previous Fibonacci numbers to the next number in the sequence
            n1 = n2
            n2 = total
        return total  

if __name__ == '__main__':
    # Run the following code until the user chooses to exit the program
    while (True):
        selected_index = input('Press q to quit or choose an index: ')
    # Exit the program if the user inputs "q"
        if selected_index == 'q':
            break
    # Run the fib() function and print the output
        try:
            idx = int(selected_index)
            print ('Fibonacci number at '
               'index {:} is {:}.'.format(idx,fib(idx)))
    # If the fib() function returns an error, let the user know that 
    # the index was invalid.
        except ValueError:
            print ('Fibonacci number at '
               'index {:} is invalid, index must be a positive integer.'
               .format(selected_index))
            
            
        
        
        