''' @file       DRV8847.py
    @brief      A driver for the motors.
    @details    The driver enables and disables the motor, senses faults, and
                sets the duty cycle with a user input.
                
                Motor position spun in the negative direction.
                \image html pos1.jpg
                
                Motor angular velocity spun in the negative direction.
                \image html speed1.jpg
                
                Motor position spun in the positive direction.
                \image html pos2.jpg
                
                Motor angular velocity spun in the positive direction.
                \image html speed2.jpg
                
    @author     Corey Agena
    @author     Austin Fong
    @date       October 19, 2021
'''

import pyb
import utime

class DRV8847:
    ''' @brief          A motor driver class for the DRV8847 from TI.
        @details        Objects of this class can be used to configure the
                        DRV8847 motor driver and to create one or moreobjects
                        of the Motor class which can be used to perform motor
                        9 control.

                        Refer to the DRV8847 datasheet here:
                        https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''
    
    def __init__ (self, pin_nsleep, pin_nfault, timer, FaultFlag):
        ''' @brief          Initializes and returns a DRV8847 object.
            @details        Initializes variable and contains enable, disable,
                            and fault callback functions.
            @param          pin_nsleep String that represents the nsleep
                            pin.
            @param          pin_nfault String that represents the nfault
                            pin.
            @param          timer The timer to use for the Motor.
            @param          FaultFlag Flag to indicate if the fault condition
                            is active or not.
        '''
        
        ## Create a pin object for nSLEEP
        self.pin_nsleep = pyb.Pin(pin_nsleep, pyb.Pin.OUT_PP)
        
        ## Create a pin object for nFault
        self.pin_nfault = pyb.Pin(pin_nfault, pyb.Pin.IN)
        self.Fault = pyb.ExtInt(self.pin_nfault, mode=pyb.ExtInt.IRQ_FALLING,
                     pull=pyb.Pin.PULL_NONE, callback=self.fault_cb)
                     
        ## Create a timer object for the motor
        self.tim = pyb.Timer(timer, freq = 20000)
        
        ## Create a flag for the fault
        self.FaultFlag = FaultFlag
        
    def enable (self):
        ''' @brief          Brings the DRV8847 out of sleep mode.
        '''
        self.Fault.disable()                  # Disable fault interrupt
        self.pin_nsleep.high()                # Re-enable the motor driver
        utime.sleep_us(25)                    # Wait for the fault pin to
                                              # return high
        self.Fault.enable()                   # Re-enable the fault interrupt

    def disable (self):
        ''' @brief          Puts the DRV8847 in sleep mode.
        '''
        self.pin_nsleep.low()

    def fault_cb (self, IRQ_src):
        ''' @brief          Callback function to run on fault condition.
            @param          IRQ_src The source of the interrupt request.
        '''
        self.disable()
        self.FaultFlag.write(True)
        print('Fault Detected')
    
    def motor (self, mot_pin1, mot_pin2, mot_tim1, mot_tim2):
        ''' @brief          Initializes and returns a motor object
                            associated with the DRV8847.
            @return         An object of class Motor
        '''
        return Motor(mot_pin1, mot_pin2, mot_tim1, mot_tim2, self.tim)

class Motor:
    ''' @brief          A motor class for one channel of the DRV8847.
        @details        Objects of this class can be used to apply PWM to a 
                        given DC motor.
    '''

    def __init__ (self, mot_pin1, mot_pin2, mot_tim1, mot_tim2, timer):
        ''' @brief          Initializes and returns a motor object associated
                            with the DRV8847.
            @details        Objects of this class should not be instantiated
                            directly. Instead create a DRV8847 object and use
                            that to create Motor objects using the method
                            DRV8847.motor().
            @param          mot_pin1 A string representing the first pin
                            relevant to the motor on the nucleo.
            @param          mot_pin2 A string representing the second pin
                            relevant to the motor on the nucleo.
            @param          mot_tim1 Integer that represents the first timer 
                            relevant to the motor on the nucleo.
            @param          mot_tim2 Integer that represents the second timer 
                            relevant to the motor on the nucleo.
            @param          timer Timer object used for the motor.
        '''
        self.tim = timer
                
        self.pin1 = pyb.Pin(mot_pin1, pyb.Pin.OUT_PP)
        self.pin2 = pyb.Pin(mot_pin2, pyb.Pin.OUT_PP)

        self.tch1 = self.tim.channel(mot_tim1, pyb.Timer.PWM, pin = self.pin1)
        self.tch2 = self.tim.channel(mot_tim2, pyb.Timer.PWM, pin = self.pin2)
    
    def set_duty (self, duty):
        ''' @brief          Set the PWM duty cycle for the motor channel.
            @details        This method sets the duty cycle to be sent
                            to the motor to the given level. Positive values
                            cause effort in one direction, negative values
                            in the opposite direction.
            @param          duty A signed number holding the duty
                            cycle of the PWM signal sent to the motor
        '''
        if duty > 0:
            self.tch1.pulse_width_percent(duty)
            self.tch2.pulse_width_percent(0)
        elif duty < 0:
            self.tch1.pulse_width_percent(0)
            self.tch2.pulse_width_percent(-duty)  
        else:
            self.tch1.pulse_width_percent(0)
            self.tch2.pulse_width_percent(0) 

if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class.
    # Any code within the if __name__ == '__main__' block will only run when
    # the script is executed as a standalone program. If the script is
    # imported as a module the code block will not run.
    
    # Create a motor driver object and two motor objects. You will need to
    # modify the code to facilitate passing in the pins and timer objects
    # needed to run the motors.
    motor_drv = DRV8847(pyb.Pin.cpu.A15,'B2',3)
    motor_1 = motor_drv.motor('B4','B5',1,2)
    motor_2 = motor_drv.motor('B0','B1',3,4)

    # Enable the motor driver
    motor_drv.enable()

    # Set the duty cycle of the first motor to 40 percent and the duty cycle
    # of the second motor to 60 percent
    motor_1.set_duty(40)
    motor_2.set_duty(60)