''' @file                lab1_page.py
    @brief               File that holds the Lab 1 documentation.
    @details             The Lab 1 documentation includes the finite state
                         machine for lab 1.
    
    @page page1          Lab 1 Finite State Machine
    
    @subsection fsm      Finite State Machine
                         \image html lab1diagram.PNG
'''