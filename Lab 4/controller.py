""" @file        controller.py
    @brief       A driver for controlling the angular velocity of the motor.
    @details     The driver updates continuously and outputs actuation values.
    @author      Corey Agena
    @author      Austin Fong
    @date        October 26, 2021
"""

class Controller:
    ''' @brief             Control angular velocity of the motors.
        @details           The Controller class can be called and used with
                           different hardware requiring closed loop control.
    '''
    def __init__(self, Kp_init, omega_ref, omega_meas, pwm_sat_low,
                 pwm_sat_high, duty):
        ''' @brief                Constructs a controller object
            @details              The initialization creates objects
                                  required for control.
            @param Kp_init        The initial proportional gain provided by
                                  the user.
            @param omega_ref      The reference angular velocity provided by
                                  the user.
            @param omega_meas     A shares.Share object for the measured
                                  angular velocity.
            @param pwm_sat_low    Lower Saturation limit on the PWM level for
                                  the current hardware.
            @param pwm_sat_high   Upper Saturation limit on the PWM level for
                                  the current hardware.
            @param duty           A shares.Share object for the duty cycle
                                  provided to the motor after control is
                                  complete.
        '''
        ## Create a proportional gain variable and set it to the initial value.
        self.Kp = Kp_init
        
        ## Create a reference angular velocity variable.
        self.omega_ref = omega_ref
        
        ## Create a measured angular velocity variable.
        self.omega_meas = omega_meas
        
        ## Create PWM saturation variables.
        self.pwm_sat_low = pwm_sat_low
        self.pwm_sat_high = pwm_sat_high
        
        ## Create a actuation variable and set it to zero.
        self.L = 0
        
        ## Create a share.Shares object for the duty cycle.
        self.duty = duty
        
    def update(self):
        ''' @brief          Calculates the actuation value.
            @details        Computes and returns the actuation value based on
                            the measured and reference values.
        '''
        if self.omega_ref.read() < 0:
            self.L = self.Kp.read()*abs(self.omega_ref.read() - self.omega_meas.read())*(-1)
        elif self.omega_ref.read() > 0:
            self.L = self.Kp.read()*abs(self.omega_ref.read() - self.omega_meas.read())
        if self.L > self.pwm_sat_high:
            self.L = self.pwm_sat_high
        elif self.L < self.pwm_sat_low:
            self.L = self.pwm_sat_low
        self.duty.write(self.L)
            
    def get_Kp(self):
        ''' @brief          Returns the proportional gain.
        '''
        return self.Kp
    
    def set_Kp(self,Kp):
        ''' @brief          Changes the proportional gain.
        '''
        self.Kp = Kp