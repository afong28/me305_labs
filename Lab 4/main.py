''' @file       main.py
    @brief      Main script for cooperative multitasking.
    @details    Implements cooperative multitasking using tasks implemented by
                finite state machines.
                
                Lab 2 task diagram.
                \image html Lab2_Task_Diagram.PNG
                
                Lab 3 task diagram.
                \image html Lab3_Task_Diagram.jpg
                
                Lab 4 task diagram.
                \image html Lab4_Task_Diagram.jpg
                
    @author     Corey Agena
    @author     Austin Fong
    @date       October 5, 2021
'''

import shares
import task_user
import task_encoder
import encoder
import DRV8847
import task_motor
import controller

def main():
    ''' @brief            The main program to run both tasks.
        @details          Creates share objects and runs the user and encoder
                          tasks.
    '''
    
    ## Create shares.Share objects to reset each encoder.
    ZeroFlag_1 = shares.Share()
    ZeroFlag_2 = shares.Share()
    
    ## Create shares.Share objects for the position of each encoder.
    position_1 = shares.Share()
    position_2 = shares.Share()
    
    ## Create shares.Share objects for delta of each encoder.
    delta_1 = shares.Share()
    delta_2 = shares.Share()
    
    ## Create shares.Share objects for the duty cycles of both motors.
    duty_1 = shares.Share(0)
    duty_2 = shares.Share(0)
    
    ## Create shares.Share objects for the faults of each object.
    FaultFlag = shares.Share(False)
    ClearFaultFlag = shares.Share(True)    
    
    ## Create shares.Share objects for the proportional gain of each
    ## controller.
    Kp_init_1 = shares.Share(0)
    Kp_init_2 = shares.Share(0)
    
    ## Create shares.Share objects for the refrence angular velocity of
    ## each motor.
    omega_ref_1 = shares.Share(0)
    omega_ref_2 = shares.Share(0)
    
    ## Create shares.Share object for each controller.
    ContFlag_1 = shares.Share(False)
    ContFlag_2 = shares.Share(False)
    
    ## Create a motor driver object and two motor objects. You will need to
    ## Create two encoder objects
    enc_1 = encoder.Encoder('B6', 'B7', 4 )
    enc_2 = encoder.Encoder('C6', 'C7', 8 )
    
    ## modify the code to facilitate passing in the pins and timer objects
    ## needed to run the motors.
    motor_drv = DRV8847.DRV8847('A15','B2',3, FaultFlag)
    motor_1 = motor_drv.motor('B4','B5',1,2)
    motor_2 = motor_drv.motor('B0','B1',3,4)
    
    ## Create a controller driver object for each motor.
    controller_1 = controller.Controller(Kp_init_1, omega_ref_1, delta_1,
                                         -100, 100, duty_1)
    controller_2 = controller.Controller(Kp_init_2, omega_ref_2, delta_2,
                                         -100, 100, duty_2)
    
    ## A user interface object for both encoders
    userPeriod = 20_000
    ## A user interface object for both encoders
    task_1 = task_user.Task_User('user', userPeriod, ZeroFlag_1, position_1, 
                                delta_1, duty_1, ContFlag_1, Kp_init_1,
                                omega_ref_1, ZeroFlag_2, position_2, delta_2,
                                duty_2, ContFlag_2, Kp_init_2, omega_ref_2,
                                FaultFlag, ClearFaultFlag)
    
    ## A encoder task object for the first encoder
    encoderPeriod = 5_000
    task_2 = task_encoder.Task_Encoder(encoderPeriod, ZeroFlag_1, position_1,
                                      delta_1, enc_1)
    
    ## A encoder task object for the second encoder
    task_3 = task_encoder.Task_Encoder(encoderPeriod, ZeroFlag_2, position_2,
                                      delta_2, enc_2)
    
    ## A motor task object for the first motor
    task_4 = task_motor.Task_Motor(encoderPeriod, motor_drv, duty_1, motor_1,
                                   controller_1, FaultFlag, ContFlag_1)
    
    ## A motor task object for the second motor
    task_5 = task_motor.Task_Motor(encoderPeriod, motor_drv, duty_2, motor_2,
                                   controller_2, FaultFlag, ContFlag_2)
    
    ## A list of tasks to run
    task_list = [task_1, task_2, task_3, task_4, task_5]
    
    while(True):
        if ClearFaultFlag.read() == True:
            motor_drv.enable()
            ClearFaultFlag.write(False)
        try:
            for task in task_list:
                task.run()
                
        except KeyboardInterrupt:
            break
    
    print('Program Terminating')

if __name__ == '__main__':
    main()
