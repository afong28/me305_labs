""" @file        task_encoder.py
    @brief       Encoder task for updating encoder interface.
    @details     Implements a finite state machine to continuosly update the
                 encoder position and zero the encoder when required.
                 
                 \image html Lab2_Task_Encoder_FSM.PNG
                 
    @author      Corey Agena
    @author      Austin Fong
    @date        October 5, 2021
"""
## All imports near the top
import utime

class Task_Encoder:
    ''' @brief      Encoder task class for updating encoder position.
        @details    Implements a finite state machine
    '''
    def __init__(self, period, ZeroFlag, position, delta, enc):    
        ''' @brief              Constructs encoder task objects.
            @details            The encoder task is implemented as a 
                                finite state machine.
            @param period       The period, in milliseconds, between runs of 
                                the task.
            @param ZeroFlag     A shares.Share object used to zero the encoder.
            @param position     A shares.Share object used to report position.
            @param delta        A shares.Share object used to report delta.
            @param enc          A encoder object that indicates which encoder
                                to run the task for.
            @param omega_meas   A shares.Share object used to update the
                                measured angular velocity.
        '''
        ## The period (in us) of the task
        self.period = period
        ## A shares.Share object used to zero the encoder
        self.ZeroFlag = ZeroFlag
        ## A shares.Share object for position
        self.position = position
        ## A shares.Share object for delta
        self.delta = delta
        ## An encoder object
        self.enc = enc
        ## Set the next_time variable for the first iteration of the code
        self.next_time = self.period
        ## Set the initial time to zero
        self.initial_time = 0
        ## The number of runs of the state machine
        self.runs = 0

        
    def run(self):
        ''' @brief         Runs one iteration of the task.
            @details       It zeros the encoder if required and updates
                           position otherwise.
        '''
        current_time = utime.ticks_us()
        if (utime.ticks_diff(current_time, self.initial_time) >=
            self.next_time):
            if self.runs == 0:
                self.initial_time = utime.ticks_us()
            elif self.ZeroFlag.read() == True:
                self.enc.set_position(0)
                self.enc.update()
                self.ZeroFlag.write(False)
                self.position.write(self.enc.get_position()/4000*2*3.1415)
            else:
                self.enc.update()
                self.position.write(self.enc.get_position()/4000*2*3.1415)
                self.delta.write(self.enc.get_delta()/4000*2*3.1415/
                                (self.period/1000_000))
            self.next_time += self.period
            self.runs += 1