""" @file        IMU.py
    @brief       A driver to read and write IMU information.
    @details     The driver updates continuously and includes methods to
                 complete important tasks.
    @author      Corey Agena
    @author      Austin Fong
    @date        November 2, 2021
"""
## All imports near the top
import pyb
import time
import struct

class IMU:
    ''' @brief             Interface with the IMU.
        @details           The BNO055 class can be called and used with
                           different IMUs.
    '''
    def __init__(self, I2C):
        ''' @brief                Constructs objects to use in the functions
            @details              The initialization creates objects as a part
                                  of self so that it can be used throughout
                                  the file.
            @param I2C            The I2C object to be modified.
        '''
        self.i2c = I2C
        
    def change_op(self, mode):
        self.i2c.mem_write(mode, 0x28, 0x3D)
        # print(self.i2c.mem_read(1, 0x28, 0x3D))
        
    def get_status(self):
        
        cal_bytes = bytearray(1)
        
        self.i2c.mem_read(cal_bytes, 0x28, 0x35) 
        
        # print("Binary:", '{:#010b}'.format(cal_bytes[0]))
        
        cal_status = (cal_bytes[0] & 0b11,
                     (cal_bytes[0] & 0b11 << 2) >> 2,
                     (cal_bytes[0] & 0b11 << 4) >> 4,
                     (cal_bytes[0] & 0b11 << 6) >> 6)

        print("Values:", cal_status)
        print('\n')
        return '{:}'.format(cal_status)
        
    def get_calib(self):
        calib_bytes = bytearray(22)
        self.i2c.mem_read(calib_bytes, 0x28, 0x55)
        return calib_bytes
        
    def set_calib(self, data):
        self.i2c.mem_write(data, 0x28, 0x55)
        
    def get_euler(self):
        eul_bytes = bytearray(6)
        self.i2c.mem_read(eul_bytes, 0x28, 0x1A)
        
        # First, unpack bytes into three signed integers
        eul_signed_ints = struct.unpack('<hhh', eul_bytes)
        
        # Second, scale ints to get proper units
        eul_vals = tuple(eul_int/16 for eul_int in eul_signed_ints)
        print('Scaled: ', eul_vals)
        
    def get_vel(self):
        vel_bytes = bytearray(6)
        self.i2c.mem_read(vel_bytes, 0x28, 0x14)
        
        # Unpack bytes into three signed integers
        vel_signed_ints = struct.unpack('<hhh', vel_bytes)
        print('Velocities: ', vel_signed_ints)
    
if __name__ == '__main__':
    i2c = pyb.I2C(1, pyb.I2C.MASTER)
    drv = IMU(i2c)
    drv.change_op(0x0C)
    while True:
        time.sleep(0.5)
        drv.get_status()
        if drv.get_status() == '(3, 3, 3, 3)':
            break
    data = drv.get_calib()
    drv.set_calib(data)
    while True:
        time.sleep(0.5)
        drv.get_euler()
#    while True:
#        time.sleep(0.5)
#        drv.get_vel()
#       i2c.mem_read(4, 0x28, 0x00)
