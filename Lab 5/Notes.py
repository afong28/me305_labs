""" @file        BNO055.py
    @brief       A driver to read and write IMU information.
    @details     The driver updates continuously and includes methods to
                 complete important tasks.
    @author      Corey Agena
    @author      Austin Fong
    @date        November 2, 2021
"""
## All imports near the top
import pyb

class BNO055:
    ''' @brief             Interface with the IMU.
        @details           The BNO055 class can be called and used with
                           different IMUs.
    '''
    def __init__(self, I2C):
        ''' @brief                Constructs objects to use in the functions
            @details              The initialization creates objects as a part
                                  of self so that it can be used throughout
                                  the file.
            @param I2C            The I2C object to be modified.
        '''
        self.i2c = I2C
        
    def change_op_mode(self):
        self.i2c.mem_write(4, 0x28, 0x3D)
            
if __name__ == '__main__':
##    i2c = I2C(1)
    i2c = pyb.I2C(1, pyb.I2C.MASTER)
    i2c.mem_write(0x0C, 0x28, 0x3D)
##    i2c.init(I2C.MASTER, baudrate=200000)
    
    i2c.mem_read(4, 0x28, 0x00)
## bytes_out = pyb.I2C(1, pyb.I2C.MASTER)
## bytes_out[0]
## bytes_out[1]
## bytes_out[2]
## bytes_out[3]
## buf = bytearray(4)
## i2c.mem_read(buf, 0x28, 0x00)
## buf[0]
## buf[1]
## buf[2]
## buf[3]