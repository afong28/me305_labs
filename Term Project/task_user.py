""" @file        task_user.py
    @brief       User interface task for the encoders, motors, and controllers.
    @details     Implements a finite state machine to recieve inputs from
                 the keyboard and execute the corresponding task.
    @author      Corey Agena
    @author      Austin Fong
    @date        October 14, 2021
"""

# All imports near the top
import utime, pyb
from array import array
from micropython import const
from ulab import numpy as np

## State 0 of the user interface task
S0_CALIB_PANEL      = const(0)
## State 1 of the user interface task
S1_CALIB_IMU        = const(1)
## State 2 of the user interface task
S2_HELP             = const(2)
## State 3 of the user interface task
S3_WAIT_FOR_CHAR    = const(3)
## State 4 of the user interface task
S4_MOTOR            = const(4)
## State 5 of the user interface task
S5_PLATE            = const(5)
## State 6 of the user interface task
S6_BALL             = const(6)
## State 7 of the user interface task
S7_DATA             = const(7)
## State 8 of the user interface task
S8_PRINT            = const(8)

class Task_User:
    ''' @brief      User class interface task for the encoders, motors, and
                    controllers.
        @details    Implements a class that can be used for other applications.
                    Task User Finite State Machine
                    \image html Final_Task_User_FSM.jpg      
    '''
            
    def __init__(self, name, period, duty_1, duty_2, b_flag, dis_flag,
                 calib_flag_tp, calib_flag_IMU, state_vect1, state_vect2,
                 dbg=False):
        ''' @brief                  Constructs a user task.
            @details                The user task is implemented as a finite
                                    state machine.
            @param name             The name of the task
            @param period           The period, in milliseconds, between runs
                                    of the task.
            @param duty_1           A shares.Share object used to set the
                                    duty cycle of motor 1.
            @param duty_2           A shares.Share object used to set the
                                    dutycycle of motor 2.
            @param b_flag           A shares.Share object used to set the
                                    controller state.
            @param dis_flag         A shares.Share object to set the duty
                                    cycles of both motors to zero.
            @param calib_flag_tp    A shares.Share object to indicate if
                                    calibration is required for the panel.
            @param calib_flag_IMU   A shares.Share object to indicate if
                                    calibration is required for the IMU.
            @param state_vect1      The state vector representing the data in
                                    the y-direction.
            @param state_vect2      The state vector representing the data in
                                    the x-direction. 
            @param dbg              A boolean flag used to enable or disable 
                                    debug messages printed over the VCP.            
        '''     
        ## The name of the task
        self.name = name
        
        ## The period (in us) of the task
        self.period = period
        
        ## shares.Share objects for duty cycle
        self.duty_1 = duty_1
        self.duty_2 = duty_2
        
        ## shares.Share objects to set the controller state
        self.b_flag = b_flag
        
        ## shares.Share object to set the duty cycle to both motors to zero.
        self.dis_flag = dis_flag
        
        ## shares.Share objects to indicate if calibration is required.
        self.calib_flag_tp = calib_flag_tp
        self.calib_flag_IMU = calib_flag_IMU

        ## shares.Share objects to represent the state vectors.
        self.state_vect1 = state_vect1
        self.state_vect2 = state_vect2
        
        ## Array objects to store actuation data
        array_size = int((30_000_000/self.period) + 1)
        self.array_L = array('f',array_size * [0])
        
        ## A flag indicating if debugging print messages display
        self.dbg = dbg
        
        ## A serial port to use for user I/O
        self.ser = pyb.USB_VCP()
        
        ## The state to run on the next iteration of the finite state machine
        self.state = S0_CALIB_PANEL
        
        ## The number of runs of the state machine
        self.runs = 0
        
        ## The variable used to report time
        self.n = 0
        
        ## Set the next_time variable for the first iteration of the code
        self.next_time = self.period
        
        ## Set the initial time to zero
        self.initial_time = 0
        
        ## Create a motor flag and set it to zero
        self.mot_flag = 0
        
        ## Create a empty string object
        self.num_str = ''
        
        ## Create a array size object
        self.size = 0
        
        ## The number of runs of the data state and write state
        self.runs_data = 0
        self.runs_write = 0
        
        ## Empty data set
        self.data = np.zeros((500,6))
        
        ## Empty string
        self.data_string = ''
        
        ## Zero time values
        self.time = 0
        self.stime = 0
        
    def run(self):
        ''' @brief         Runs one iteration of the task.
            @details       Waits for a command and executes based on 
                           the typed letter.
        '''
        current_time = utime.ticks_us()
        if (utime.ticks_diff(current_time, self.initial_time) >=
            self.next_time):
            if self.runs == 0:
                self.initial_time = utime.ticks_us()
                
            elif self.state == S0_CALIB_PANEL:
                if self.calib_flag_tp.read() == 1:
                    self.transition_to(S1_CALIB_IMU)
            
            elif self.state == S1_CALIB_IMU:
                if self.calib_flag_IMU.read() == 1:
                    self.transition_to(S2_HELP)
                
            elif self.state == S2_HELP:
                print('Welcome, press:'
                      '\n\'b\' to start balancing the plate'
                      '\n\'d\' to set the both motors to a duty cycle of 0'
                      '\n\'m\' to prompt the user to enter a duty cycle for'
                      ' motor 1'
                      '\n\'M\' to prompt the user to enter a duty cycle for'
                      ' motor 2'
                      '\n\'g\' to collect state vector data'
                      '\n\'s\' to end data collection prematurely'
                      '\n\'h\' return to the welcome screen')
                self.transition_to(S3_WAIT_FOR_CHAR)
                
            elif self.state == S3_WAIT_FOR_CHAR:
                if self.ser.any():
                    char_in = self.ser.read(1).decode()
                    if(char_in == 'b' or char_in == 'B'):
                        self.dis_flag.write(False)
                        self.b_flag.write(1)
                        print('Balancing Platform')
                        print('Press:'
                              '\n\'b\' to start balancing the ball'
                              '\n\'d\' to set the both motors to a duty cycle'
                              ' of 0 and return to the help menu')
                        self.transition_to(S5_PLATE)
                    elif(char_in == 'd' or char_in == 'D'):
                        self.dis_flag.write(True)
                        self.b_flag.write(0)
                        self.duty_1.write(0)
                        self.duty_2.write(0)
                    elif(char_in == 'm'):
                        self.dis_flag.write(False)
                        print('Please enter the desired duty cycle'
                              ' for motor 1:')
                        self.mot_flag = 0
                        self.transition_to(S4_MOTOR)
                    elif(char_in == 'M'):
                        self.dis_flag.write(False)
                        print('Please enter the desired duty cycle'
                              ' for motor 2:')
                        self.mot_flag = 1
                        self.transition_to(S4_MOTOR)
                    elif(char_in == 'g' or char_in == 'G'):
                        self.transition_to(S7_DATA)
                    elif(char_in == 'h' or char_in == 'H'):
                        self.transition_to(S2_HELP)
                    else:
                        print('Command \'{:}\' is invalid.'.format(char_in))
                    
            elif self.state == S4_MOTOR:
                if self.ser.any():
                    char_in = self.ser.read(1).decode()
                    if char_in.isdigit():
                        self.ser.write(char_in)
                        self.num_str += char_in
                    elif char_in == '-':
                        if self.num_str == '':
                            self.ser.write(char_in)
                            self.num_str += char_in
                    elif char_in == '\b' or char_in == '\x7F':
                        self.ser.write('\x7F')
                        if self.num_str != '':
                            self.num_str = self.num_str[:-1]
                    elif char_in == '.':
                        if '.' not in self.num_str:
                            self.ser.write(char_in)
                            self.num_str += char_in
                    elif char_in == '\r' or char_in == '\n':
                        self.ser.write('\r')
                        if self.num_str != '':
                            char_f = float(self.num_str)
                            if char_f <= 100 and char_f >= -100:
                                if self.mot_flag == 0:
                                    self.duty_1.write(char_f)
                                    print('Duty cycle for motor 1 is'
                                          ' set to {:}'.format(self.num_str))
                                elif self.mot_flag == 1:
                                    self.duty_2.write(char_f)
                                    print('Duty cycle for motor 2 is'
                                          ' set to {:}'.format(self.num_str))
                                self.num_str = ''
                                self.transition_to(S3_WAIT_FOR_CHAR)                            
                                print('Enter a new command, pressing h will'
                                      ' return you to the welcome screen.')
                            else:
                                print('Duty cycle must be between -100 and'
                                      ' 100\nPlease enter a duty cycle:')
                                self.num_str = ''
                        else:
                            print('No duty cycle provided,'
                                  ' please enter a duty cycle:')
                
            elif self.state == S5_PLATE:
                if self.ser.any():
                    char_in = self.ser.read(1).decode()
                    if(char_in == 'b' or char_in == 'B'):
                        self.dis_flag.write(False)
                        self.b_flag.write(2)
                        print('Balancing Platform and Ball')
                        print('Press:'
                              '\n\'b\' or \'d\' to stop balancing the ball and'
                              ' return to the help menu'
                              '\n\'g\' to start data collection')
                        self.transition_to(S6_BALL)
                    elif(char_in == 'd' or char_in == 'D'):
                        self.dis_flag.write(True)
                        self.b_flag.write(0)
                        self.duty_1.write(0)
                        self.duty_2.write(0)
                        self.b_flag.write(0)
                        self.transition_to(S2_HELP)
              
            elif self.state == S6_BALL:
                if self.ser.any():
                    char_in = self.ser.read(1).decode()
                    if(char_in == 'b' or char_in == 'B'):
                        self.dis_flag.write(True)
                        self.b_flag.write(0)
                        self.duty_1.write(0)
                        self.duty_2.write(0)
                        self.runs.data = 0
                        self.transition_to(S2_HELP)
                    if(char_in == 'd' or char_in == 'D'):
                        self.dis_flag.write(True)
                        self.b_flag.write(0)
                        self.duty_1.write(0)
                        self.duty_2.write(0)
                        self.transition_to(S2_HELP)
                    elif(char_in == 'g' or char_in == 'G'):
                        self.transition_to(S7_DATA)
                        
            elif self.state == S7_DATA:
                if self.runs_data == 0:
                    self.stime = utime.ticks_us()
                    print('Begin Data Collection')
                    print('Press: \'d\' to stop data collection')
                    
                ## Collect timer values.
                self.time = utime.ticks_us()
                if self.time - self.stime <= 60_000_000:
                    x = self.state_vect1[0].read()
                    y = self.state_vect2[0].read()
                    theta_x = self.state_vect2[1].read()
                    theta_y = self.state_vect1[1].read()
                    duty_1 = self.duty_1.read()
                    duty_2 = self.duty_2.read()
                    runs_data = self.runs_data - 1
                    self.data[runs_data,0] = x
                    self.data[runs_data,1] = y
                    self.data[runs_data,2] = theta_x
                    self.data[runs_data,3] = theta_y
                    self.data[runs_data,4] = duty_1
                    self.data[runs_data,5] = duty_2
                if self.ser.any():
                    char_in = self.ser.read(1).decode()
                    if(char_in == 'd' or char_in == 'D' or self.time - self.stime > 60_000_000):
                        self.runs_write = 0
                        self.transition_to(S8_PRINT)
                self.runs_data += 1

            elif self.state == S8_PRINT:
                self.dis_flag.write(True)
                self.b_flag.write(0)
                self.duty_1.write(0)
                self.duty_2.write(0)
                for i in range(0,self.runs_data):
                    print("{:},{:},{:},{:},{:},{:},{:}".format(round(self.n, 3),
                                self.data[self.runs_write,0],
                                self.data[self.runs_write,1],
                                self.data[self.runs_write,2],
                                self.data[self.runs_write,3],
                                self.data[self.runs_write,4],
                                self.data[self.runs_write,5]))
                    self.n += self.period/1000000
                self.transition_to(S2_HELP)                    
            else:
                raise ValueError('Invalid State')
            
            self.next_time += self.period
            self.runs += 1
            
    def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state.
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        if (self.dbg):
            print('{:}: S{:}->S{:}'.format(self.name,self.state,new_state))
        self.state = new_state