''' @file                report_page.py
    @brief               File that holds a report of the ball balance platform.
    @details             The report documentation includes a description of the 
                         various features, calculations, diagrams, analysis, 
                         plots, and demonstrations of the platform
    
    @page page2          Term Project Report
    
    @subsection info     Description
                         The balancing platform has two stages. When the user
                         presses 'b' once, the platform orients itself to be
                         flat according to the readings taken from the IMU
                         module on the corner. If the user pushes down on any
                         part of the platform, the platform will readjust 
                         itself. When the the user presses 'b' again, the touch
                         panel will activate. Now, both the readings from the 
                         IMU and touch panel will be used to balance the 
                         platform. For an in-depth explanation on how the duty
                         cycle for each motor is determined, including analysis
                         and calculations, see the report linked below.
                         
                         <A HREF = "Ball_Balancer_System_Modeling.html">
                         <B>Ball Balancer System Modeling</B></A>
                         
                         Once the ball is balancing, the user may also press
                         'g' to begin collecting data of the state vector over
                         time. The state vector includes the x and y
                         coordinates, angles about the x and y axis, and the
                         duty cycle for each motor. This data can then be 
                         graphed (see "Plots" section). At any point during the
                         balancing or data collection process, the user may 
                         press 'd' to disable the motors and return to the home
                         menu.
                         
                         For debugging purposes, the user may also press 'm' or
                         'M' to manually enter a duty cycle for motor 1 or
                         motor 2, respectively.

    @subsection dem      Demonstration
                         A demonstration of both the balancing platform and
                         ball can be found below. Commentary is provided for
                         the ball balancing video. The motors and touch panel 
                         were difficult to work with, so the ball balancing
                         demonstration does not balance for too long. The
                         platform-only balancing demonstration is not
                         significantly affected by this because the balancing 
                         does not have to be immediate. Balancing the ball 
                         requires precise and quick movements of the motor.
                         Additionally, a video demonstrating the collection
                         of data can be found below. The video includes
                         another attempt at balancing the ball.
                         
                         <A HREF = "https://youtu.be/znxpRkj665c">
                         <B>User Interface, Plate Balancing, and Ball Balancing Video</B></A>
                         
                         <A HREF = "https://youtu.be/xCNRucXcweQ">
                         <B>Data Gathering Demonstration Video</B></A>
                         
    @subsection calib    Calibration
                         Both the touch panel and IMU need to be calibrated
                         before accurate measurements can be taken. Both 
                         processes write the calibration coefficients to a
                         text file stored in the same directory as the task
                         files. If a file is detected, no calibration is
                         necessary. A video demonstration of the calibration
                         process for both the touch panel and IMU is linked 
                         below.
                         
                         <A HREF = "https://youtu.be/Oirh8C8DGks">
                         <B>Calibration of Touch Panel and IMU Video</B></A>
                         
    @subsection tasks    Tasks
                         Four tasks are run during the ball balancing platform
                         process. Every task is ran through the main file, 
                         main.py. The tasks include: user, motor, panel, and
                         IMU tasks.
                         
                         All four tasks and their interactions with shares
                         are represented by the task diagram below. The dashed
                         lines represent shares between the tasks. A
                         description for each share is shown to the right of 
                         the diagram.
                         
                         \image html Final_Task_Diagram.jpg
                         
                         The touch panel task interfaces with the touch panel
                         driver. The first state calibrates the touch panel
                         with user feedback. Once calibration is complete, the
                         task reads the measurements from the touch panel and
                         writes these values into their corresponding locations
                         into the state vector shares. The values include x and
                         y coordinates and velocity in the x and y direction.
                         
                         \image html Final_Task_Panel_FSM.jpg
                         
                         The IMU task is similar to the touch panel task,
                         except it interfaces with the IMU driver instead.
                         The first state calibrates the IMU with user feedback. 
                         Once calibration is complete, the task reads the 
                         measurements from the IMU and writes these values into
                         their corresponding locations in the state vector 
                         shares. The values include the angles and angular 
                         velocity about the x and y axis.
                         
                         \image html Final_Task_IMU_FSM.jpg
                         
                         The motor task runs the motor 1 or 2 at the duty
                         cycle provided by the user or the controller. The 
                         controller driver is only called when the ball 
                         balancing command 'b' is pressed.
                         
                         \image html Final_Task_Motor_FSM.jpg
                         
                         Finally, the user task allows the user to interact
                         with the IMU, motor, and touch panel. The first two 
                         states, S0 and S1, calibrate the touch panel and IMU
                         when necessary. The third state, S2, prompts the user 
                         with allowable commands. This message can be shown
                         at any time whenever the user presses 'h'. Immediately
                         after this state, the fourth state, S3, waits for the
                         user to input a command. If the user presses either
                         'm' or 'M', state five, S4, is initiated. Here, the 
                         user can input a duty cycle for either motor 1 or 
                         motor 2. However, if the user presses 'b' in S3, the
                         ball balancing states will be run. S5 balances with
                         only the readings from the IMU, while S6 balances with
                         both the IMU and touch panel readings. During S6, the
                         user may also press 'g' to begin the data collection
                         phase S7 which records the state vector over time. S8
                         prints the data to the REPL and transitions back to
                         the help state S2.
                         
                         \image html Final_Task_User_FSM.jpg
'''
